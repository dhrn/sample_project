import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {MISModule} from './mis/mis.module';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import {Routing} from './routing';

import { AngularFireModule } from 'angularfire2';

// New imports to update based on AngularFire2 version 4
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

export const firebaseConfig = {
  apiKey: "AIzaSyB4g3Qi4duqiTz6lCtrcmqxIewOFrWfM0o",
  authDomain: "angular-sample-project.firebaseapp.com",
  databaseURL: "https://angular-sample-project.firebaseio.com",
  projectId: "angular-sample-project",
  storageBucket: "angular-sample-project.appspot.com",
  messagingSenderId: "767927816863"
};

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    MISModule,
    ReactiveFormsModule , FormsModule,
    Routing,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
