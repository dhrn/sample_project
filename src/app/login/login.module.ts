import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import {Routinglogin} from './routinglogin';

@NgModule({
  imports: [
    CommonModule , Routinglogin
  ],
  declarations: [LoginComponent, RegistrationComponent]
})
export class LoginModule { }
