import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {LoginComponent} from './login/login.component';
import {RegistrationComponent} from './registration/registration.component';
const Loginroutes: Routes = [
    { path: 'login',  component: LoginComponent},
    { path: 'registration', component: RegistrationComponent }
];

@NgModule
({
    imports: [
        RouterModule.forChild(Loginroutes)
    ],
    exports: [
        RouterModule
    ]
})

export class Routinglogin {
}
