import { Component, OnInit } from '@angular/core';
import {Users} from '../users';

@Component({
  selector: 'app-get-all',
  templateUrl: './get-all.component.html',
  styleUrls: ['./get-all.component.css'],
  providers: [ Users ]
})
export class GetAllComponent implements OnInit {

  employees;
  selected;
  getone;
  constructor(private emp: Users) { console.log('constructure');}

  ngOnInit() {
    this.employees = this.emp.getalluser();
  }
  delete(emp: string) {
    console.log('Deleted');
    this.emp.delete();
  }
  onSelect(emp) {
  this.selected = emp.name;
  console.log('clicked', this.selected);
  }
  gotoDetail() {
    console.log("Selected", this.selected );
    console.log(this.emp.getspecific(this.selected), 'from service');
    this.getone = this.emp.getspecific(this.selected);
  }
}
