import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GetAllComponent } from './get-all/get-all.component';
import { AdduserComponent } from './adduser/adduser.component';
import { RemoveComponent } from './remove/remove.component';
import {MISRouting} from './misrouting';
import {Users} from './users';

@NgModule({
  imports: [
    CommonModule , MISRouting
  ],
  declarations: [
      GetAllComponent, AdduserComponent, RemoveComponent
  ],
  providers: [ Users ]
})
export class MISModule { }
