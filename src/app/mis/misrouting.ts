import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdduserComponent} from './adduser/adduser.component';
import {GetAllComponent} from './get-all/get-all.component';
import {RemoveComponent} from './remove/remove.component';
const adminRoutes: Routes = [
    {path: 'adduser', component: AdduserComponent },
    {path: 'getuser', component: GetAllComponent },
    {path: 'removeuser',  component: RemoveComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(adminRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class MISRouting {
}
