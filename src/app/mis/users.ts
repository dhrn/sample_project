import {Injectable} from '@angular/core';
@Injectable()
export class Users {
   public employees= [
        {id: 1 , name: 'Poo', age: 22 },
        {id: 2 , name: 'sow', age: 23 },
        {id: 3 , name: 'Mohu', age: 92 }
    ];
    getalluser() {
        return this.employees;
    }
    getspecific(name: string) {
       for (let emp of this.employees){
           if(emp.name == name) {
               return emp;
           }
       }
        return 'NOT FOUND' ;
    }
    delete() {
     this.employees.pop();
    }
    insert(user: any) {
        this.employees.push(user);
    }
}
